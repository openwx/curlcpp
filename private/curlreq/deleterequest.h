#pragma once

#include "getrequest.h"

namespace CurlCppLib
{

class DeleteRequest : public GetRequest
{
public:
    DeleteRequest();
    ~DeleteRequest();

protected:
    void CostumRequest(CURL* curl) override;
};
}  // namespace CurlCppLib