#pragma once

#include "getrequest.h"
#include <wxjson.h>

namespace CurlCppLib
{

class PutJsonRequest : public GetRequest
{
public:
    PutJsonRequest();
    ~PutJsonRequest();

    CurlCppLib::Response startReqest(const std::string& url, const std::map<std::string, std::string>& postfields, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files = {}) override;

private:
};
}  // namespace CurlCppLib