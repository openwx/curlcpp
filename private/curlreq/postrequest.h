#pragma once

#include "getrequest.h"

namespace CurlCppLib
{

class PostRequest : public GetRequest
{
public:
    PostRequest();
    ~PostRequest();

protected:
    void SetFields(CURL* curl, curl_mime* mime, curl_mimepart* part, const std::map<std::string, std::string>& postfields, const std::vector<std::tuple<std::string, std::string>>& files = {}) override;
};
}  // namespace CurlCppLib
