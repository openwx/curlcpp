#pragma once

#include "postrequest.h"

namespace CurlCppLib
{

class PutRequest : public PostRequest
{
public:
    PutRequest();
    ~PutRequest();

protected:
    void CostumRequest(CURL* curl) override;
};

}  // namespace CurlCppLib