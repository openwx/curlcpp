#pragma once

#include "../response.h"
#include <thread>
#include <future>
#include <queue>
#include <map>
#include <algorithm>
#include <vector>
#include <tuple>

namespace CurlCppLib
{

class GetRequest
{
public:
    GetRequest();
    ~GetRequest();
    virtual void SetTimeout(const long & timeout);

    virtual CurlCppLib::Response startReqest(const std::string& url, const std::map<std::string, std::string>& postfields, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files = {});

protected:
    long timeout_ = 5L;

    static size_t WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp);

    virtual void SetFields(CURL* curl, curl_mime* mime, curl_mimepart* part, const std::map<std::string, std::string>& postfields, const std::vector<std::tuple<std::string, std::string>>& files = {}){};

    virtual void CostumRequest(CURL* curl){};
};
}  // namespace CurlCppLib