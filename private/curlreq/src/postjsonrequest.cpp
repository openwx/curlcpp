#include "postjsonrequest.h"

namespace CurlCppLib
{

PostJsonRequest::PostJsonRequest()
{
}

PostJsonRequest::~PostJsonRequest()
{
}

CurlCppLib::Response PostJsonRequest::startReqest(const std::string& url, const std::map<std::string, std::string>& postfields, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files)
{
    std::string str_response;
    std::string text;
    long response_code{};
    Json parameters(postfields);
    std::string jsonPayload = parameters.dump();
    CURLcode res;
    CURL* curl;
    curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);  // Включаем HTTPS
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        /* завершено в течение 20 секунд */
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout_);

        /* отправьте все данные в эту функцию  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str_response);

        struct curl_slist* headers = NULL;

        if (!header.empty())
        {
            for (std::string itemHead : header)
            {
                // std::cout << itemHead << std::endl;
                headers = curl_slist_append(headers, itemHead.c_str());
            }

            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        }

        if (!postfields.empty())
        {
            const char* data = jsonPayload.c_str();
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
        }

        res = curl_easy_perform(curl);

        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

        /* Check for errors */
        if (res != CURLE_OK)
        {
            text = curl_easy_strerror(res);
        }
        else
        {
            text = str_response;
        }
    }
    curl_easy_cleanup(curl);

    CurlCppLib::Response retRes;
    retRes.text = text;
    retRes.status_code = response_code;
    return retRes;
}

}  // namespace CurlCppLib