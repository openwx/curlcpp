#include "getrequest.h"

namespace CurlCppLib
{
GetRequest::GetRequest()
{
}

GetRequest::~GetRequest()
{
}
void GetRequest::SetTimeout(const long& timeout)
{
    timeout_ = timeout;
}
size_t GetRequest::WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}
CurlCppLib::Response GetRequest::startReqest(const std::string& url, const std::map<std::string, std::string>& postfields, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files)
{
    std::string str_response;
    CURLcode res;

    CURL* curl;
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);  // Включаем HTTPS
    curl_mime* mime;
    curl_mimepart* part;
    mime = curl_mime_init(curl);

    // POST Поля
    SetFields(curl, mime, part, postfields, files);
    
    CostumRequest(curl);

    /* отправьте все данные в эту функцию  */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

    /* завершено в течение 20 секунд */
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout_);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str_response);

    /* example.com is redirected, so we tell libcurl to follow redirection */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    struct curl_slist* headers = NULL;
    if (!header.empty())
    {
        for (std::string itemHead : header)
        {
            // std::cout << itemHead << std::endl;
            headers = curl_slist_append(headers, itemHead.c_str());
        }

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    }

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);

    long response_code{};
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

    std::string text;
    /* Check for errors */
    if (res != CURLE_OK)
    {
        text = curl_easy_strerror(res);
    }
    else
    {
        text = str_response;
    }
    curl_mime_free(mime);

    /* always cleanup */
    curl_easy_cleanup(curl);

    CurlCppLib::Response retRes;
    retRes.text = text;
    retRes.status_code = response_code;
    return retRes;
}
}  // namespace CurlCppLib