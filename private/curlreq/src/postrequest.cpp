#include "postrequest.h"

namespace CurlCppLib
{
PostRequest::PostRequest()
{
}

PostRequest::~PostRequest()
{
}
void PostRequest::SetFields(CURL* curl, curl_mime* mime, curl_mimepart* part, const std::map<std::string, std::string>& postfields, const std::vector<std::tuple<std::string, std::string>>& files)
{
    if (postfields.size() > 0)
    {
        for (const auto& itemPost : postfields)
        {
            part = curl_mime_addpart(mime);
            curl_mime_name(part, itemPost.first.c_str());
            curl_mime_data(part, itemPost.second.c_str(), CURL_ZERO_TERMINATED);
        }
    }

    if (!files.empty())
    {
        for (const auto& iteFile : files)
        {
            part = curl_mime_addpart(mime);
            curl_mime_name(part, std::get<0>(iteFile).c_str());
            curl_mime_filedata(part, std::get<1>(iteFile).c_str());
        }

        // curl_mime_type(part, "image/jpeg");
    }

    curl_easy_setopt(curl, CURLOPT_MIMEPOST, mime);
}
}  // namespace CurlCppLib