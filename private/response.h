#pragma once

#include <curl/curl.h>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

namespace CurlCppLib
{
class Response
{
public:
    Response();
    ~Response();

    long status_code{};
    std::string text{};

private:
};

}