#include "curlcpp.h"

CurlCpp::CurlCpp()
{
}

CurlCpp::~CurlCpp()
{
}

void CurlCpp::SetTimeout(const long& timeout)
{
    timeout_ = timeout;
}

CurlCppLib::Response CurlCpp::GetRequest(const std::string& url)
{
    CurlCppLib::GetRequest req;
    std::map<std::string, std::string> data;
    std::vector<std::string> header;

    return SendRequest(req, url, data, header);
}

CurlCppLib::Response CurlCpp::GetRequest(const std::string& url, const std::vector<std::string>& header)
{
    CurlCppLib::GetRequest req;
    std::map<std::string, std::string> data;

    return SendRequest(req, url, data, header);
}

CurlCppLib::Response CurlCpp::PostRequest(const std::string& url, const std::map<std::string, std::string>& postData)
{
    CurlCppLib::PostRequest req;
    std::vector<std::string> header;

    return SendRequest(req, url, postData, header);
}

CurlCppLib::Response CurlCpp::PostRequest(const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header)
{
    CurlCppLib::PostRequest req;

    return SendRequest(req, url, postData, header);
}

CurlCppLib::Response CurlCpp::PostJsonRequest(const std::string& url, const Json& json_data, const std::vector<std::string>& header)
{
    CurlCppLib::PostJsonRequest req;

    return SendRequest(req, url, json_data, header);
}

CurlCppLib::Response CurlCpp::PostRequest(const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files)
{
    CurlCppLib::PostRequest req;

    return SendRequest(req, url, postData, header, files);
}

CurlCppLib::Response CurlCpp::PutRequest(const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header)
{
    CurlCppLib::PutRequest req;

    return SendRequest(req, url, postData, header);
}

CurlCppLib::Response CurlCpp::PutRequest(const std::string& url, const std::map<std::string, std::string>& postData)
{
    std::vector<std::string> header;
    CurlCppLib::PutRequest req;

    return SendRequest(req, url, postData, header);
}

CurlCppLib::Response CurlCpp::PutJsonRequest(const std::string& url, const Json& json_data, const std::vector<std::string>& header)
{
    CurlCppLib::PostJsonRequest req;

    return SendRequest(req, url, json_data, header);
}

CurlCppLib::Response CurlCpp::DeleteRequest(const std::string& url, const std::vector<std::string>& header)
{
    std::map<std::string, std::string> data;
    CurlCppLib::DeleteRequest req;

    return SendRequest(req, url, data, header);
}

CurlCppLib::Response CurlCpp::DeleteRequest(const std::string& url)
{
    std::vector<std::string> header;
    std::map<std::string, std::string> data;
    CurlCppLib::DeleteRequest req;

    return SendRequest(req, url, data, header);
}

CurlCppLib::Response CurlCpp::SendRequest(CurlCppLib::GetRequest& req, const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files)
{
    std::future<CurlCppLib::Response> fut = std::async(std::launch::async, [&req, url, postData, header, files, this] {
        req.SetTimeout(timeout_);
        CurlCppLib::Response ret = req.startReqest(url, postData, header, files);
        return ret;
    });

    CurlCppLib::Response ret = fut.get();
    return ret;
}
