## curlcpp

## Подключение:

### CMakeLists.txt

```plaintext
add_subdirectory(include/curlcppnet)

...

target_link_libraries(${PROJECT_NAME} curlcpp)
```

## Использование

### GET

```cpp
#include <curlcpp.h>

CurlCpp lCurl;
auto res = lCurl.GetRequest("url");
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

### GET HEADER

```cpp
#include <curlcpp.h>

std::vector<std::string> header;
header.push_back("X-CMC_PRO_API_KEY: XXXXXXX");
CurlCpp lCurl;
auto res = lCurl.GetRequest("url", header);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

### POST

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["name"] = "data";

CurlCpp lCurl;
auto res = lCurl.PostRequest("url", postData);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

#### Пример Telegram Text

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["chat_id"] = "id_chat";
postData["text"] = "text";

std::string urlBot = "https://api.telegram.org/bot<Token-Bot>/sendMessage";

auto ln = std::async(std::launch::async, [urlBot, postData] {
    CurlCpp lCurl;
    auto res = lCurl.PostRequest(urlBot, postData);

    std::cout << res.text << std::endl;
    std::cout << res.status_code << std::endl;
});
```

### POST HEADER

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["name"] = "data";

std::vector<std::string> header;
header.push_back("X-CMC_PRO_API_KEY: XXXXXXX");

CurlCpp lCurl;
auto res = lCurl.PostRequest("url", postData, header);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

### POST HEADER (Отправка файлов)

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["name"] = "data";

std::vector<std::string> header;
header.push_back("Content-Type: multipart/form-data");

std::vector<std::tuple<std::string, std::string>> files;
files.push_back({"photo", "test.jpg"});

CurlCpp lCurl;
auto res = lCurl.PostRequest("url", postData, header, files);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

#### Пример Telegram ФОТО

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["chat_id"] = "id_chat";
postData["caption"] = "text";

std::vector<std::tuple<std::string, std::string>> files;
files.push_back({"photo", "test.jpg"});

std::string urlBot = "https://api.telegram.org/bot<Token-Bot>/sendPhoto";

auto ln = std::async(std::launch::async, [urlBot, postData, files] {
    std::vector<std::string> header;
    header.push_back("Content-Type: multipart/form-data");

    CurlCpp lCurl;
    auto res = lCurl.PostRequest(urlBot, postData, header, files);

    std::cout << res.text << std::endl;
    std::cout << res.status_code << std::endl;
});
```

### PUT

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["name"] = "data";

CurlCpp lCurl;
auto res = lCurl.PutRequest("url", postData);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

### PUT HEADER

```cpp
#include <curlcpp.h>

std::map<std::string, std::string> postData;
postData["name"] = "data";

std::vector<std::string> header;
header.push_back("X-CMC_PRO_API_KEY: XXXXXXX");

CurlCpp lCurl;
auto res = lCurl.PutRequest("url", postData, header);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```


### DELETE

```cpp
#include <curlcpp.h>

CurlCpp lCurl;
auto res = lCurl.DeleteRequest("url");
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```

### DELETE HEADER

```cpp
#include <curlcpp.h>

std::vector<std::string> header;
header.push_back("X-CMC_PRO_API_KEY: XXXXXXX");

CurlCpp lCurl;
auto res = lCurl.DeleteRequest("url", header);
std::cout << res.text << std::endl;
std::cout << res.status_code << std::endl;
```