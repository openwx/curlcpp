#pragma once

#include "../private/response.h"
#include <chrono>
#include <thread>
#include <future>
#include "../private/curlreq/getrequest.h"
#include "../private/curlreq/postrequest.h"
#include "../private/curlreq/putrequest.h"
#include "../private/curlreq/deleterequest.h"
#include "../private/curlreq/postjsonrequest.h"

class CurlCpp
{
public:
    CurlCpp();
    ~CurlCpp();

    void SetTimeout(const long& timeout);

    CurlCppLib::Response GetRequest(const std::string& url);                                          // Отправка GET запроса
    CurlCppLib::Response GetRequest(const std::string& url, const std::vector<std::string>& header);  // Отправка GET запроса

    CurlCppLib::Response PostRequest(const std::string& url, const std::map<std::string, std::string>& postData);                                          // Отправка POST запроса
    CurlCppLib::Response PostRequest(const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header);  // Отправка POST запроса

    CurlCppLib::Response PostJsonRequest(const std::string& url, const Json& json_data, const std::vector<std::string>& header);  // Отправка POST Json запроса

    CurlCppLib::Response PostRequest(const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files);  // Отправка POST запроса File

    CurlCppLib::Response PutRequest(const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header);  // Отправка PUT запроса
    CurlCppLib::Response PutRequest(const std::string& url, const std::map<std::string, std::string>& postData);                                          // Отправка PUT запроса

    CurlCppLib::Response PutJsonRequest(const std::string& url, const Json& json_data, const std::vector<std::string>& header);  // Отправка POST Json запроса

    CurlCppLib::Response DeleteRequest(const std::string& url, const std::vector<std::string>& header);  // Отправка DELETE запроса
    CurlCppLib::Response DeleteRequest(const std::string& url);                                          // Отправка DELETE запроса

private:
    CurlCppLib::Response SendRequest(CurlCppLib::GetRequest& req, const std::string& url, const std::map<std::string, std::string>& postData, const std::vector<std::string>& header, const std::vector<std::tuple<std::string, std::string>>& files = {});

    long timeout_ = 5L;
};